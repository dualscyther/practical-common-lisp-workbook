(in-package :cl-user)

(load (compile-file "main.lisp"))

(save-lisp-and-die "mainexecutable"
                   :toplevel #'main
                   :executable t)
