(defvar *db* nil)
(setf *db* NIL)

(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))
(defun add-record (cd)
  (push cd *db*))
(defun dump-db ()
    (format t "~{~{~a:~10t~a~%~}~%~}" *db*))
(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))
(defun prompt-for-cd ()
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   (y-or-n-p "Ripped [y/n]: ")))
(defun add-cds ()
  (loop (add-record (prompt-for-cd))
        (if (not (y-or-n-p "Another? [y/n]: ")) (return))))
(defun save-db (filename)
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))
(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

(add-record (make-cd "Hot Fuss" "The Killers" 5 t))
(add-record (make-cd "Sam's Town" "The Killers" 5 t))
(add-record (make-cd "Sawdust" "The Killers" 5 t))
(add-record (make-cd "Day & Age" "The Killers" 5 t))
(add-record (make-cd "Melodrama" "Lorde" 5 t))
(dump-db)
(save-db "./my.db")
(load-db "./my.db")


;; Querying the Database
(remove-if-not #'evenp '(1 2 3 4 5 6 7 8 9 10))
(remove-if-not #'(lambda (x) (= 1 (mod x 2))) '(1 2 3 4 5 6 7 8 9 10))

(defun select (selector-fn)
  (remove-if-not selector-fn *db*))
(select #'(lambda (cd) (equal (getf cd :artist) "Lorde")))

(defun where (&key title artist rating (ripped nil ripped-p))
  #'(lambda (cd)
      (and
       (if title (equal (getf cd :title) title) t)
       (if artist (equal (getf cd :artist) artist) t)
       (if rating (equal (getf cd :rating) rating) t)
       (if ripped-p (equal (getf cd :ripped) ripped) t))))
(select (where :artist "Lorde"))

;; Updating Existing Records--Another Use for WHERE
(defun update (selector-fn &key title artist rating (ripped nil ripped-p))
  (setf *db*
        (mapcar
         #'(lambda (row)
             (when (funcall selector-fn row)
               (if title (setf (getf row :title) title))
               (if artist (setf (getf row :artist) artist))
               (if rating (setf (getf row :rating) rating))
               (if ripped-p (setf (getf row :ripped) ripped)))
             row) *db*)))
(update (where :artist "Lorde") :rating 10)

(defun delete-rows (selector-fn)
  (setf *db* (remove-if selector-fn *db*)))
(delete-rows (where :artist "Lorde"))
(add-record (make-cd "Melodrama" "Lorde" 5 t))

;; Removing Duplication and Winning Big
(reverse '(1 2 3))
(defmacro backwards (expr) (reverse expr))
(backwards ("hello, world" t format))

(defun make-comparison-expr (field value)
  `(equal (getf cd ,field) ,value))
(make-comparison-expr :artist "Lorde")
`(1 2 (+ 1 2))
`(1 2 ,(+ 1 2))
'(1 2 (+ 1 2))
(let ((x '(+ 3 4))) `(1 2 ,x))
(let ((x '(+ 3 4))) `(1 2 ,@x))

`(and ,(list 1 2 3))
`(and ,@(list 1 2 3))
`(and ,@(list 1 2 3) 4)

(defun make-comparisons-list (fields)
  (loop while fields
        collecting (make-comparison-expr (pop fields) (pop fields))))
(defmacro where (&rest clauses)
  `#'(lambda (cd) (and ,@(make-comparisons-list clauses))))
(macroexpand-1 '(where :title "Give Us a Break" :ripped t))
(select (where :title "Melodrama" :ripped t))

;;; 5. Functions

(defun plot (fn min max step)
  (loop for i from min to max by step do
    (loop repeat (funcall fn i) do (format t "*"))
    (format t "~%")))
(plot #'exp 0 4 1/2)
(apply #'plot (list #'exp 0 4 1/2))
;; (apply #'plot '(exp 0 4 1/2))
(apply #'plot #'exp '(0 4 1/2))

(funcall #'exp 2)
(funcall 'exp 2)

;; these two are different obviously, but why does funcall work on both?
;; turns out if the first argument to funcall is a symbol, it is coerced to a function
;; <http://clhs.lisp.se/Body/f_funcal.htm>
(type-of 'exp)
(type-of #'exp)

;;; 6. Variables

;; Dynamic, a.k.a. Special, Variables
(defun foo-let-bindings(x)
  (format t "Parameter: ~a~%" x)
  (let ((x 2))
    (format t "Outer LET: ~a~%" x)
    (let ((x 3))
      (format t "Inner LET: ~a~%" x))
    (format t "Outer LET: ~a~%" x))
  (format t "Parameter: ~a~%" x))
(foo-let-bindings 1)

(let* ((x 10)
       (y (+ x 10)))
  (list x y))

(defparameter *counter-fn* (let ((count 0))
                             (list
                              #'(lambda () (incf count))
                              #'(lambda () (decf count))
                              #'(lambda () count))))
(funcall (first *counter-fn*))
(funcall (second *counter-fn*))
(funcall (third *counter-fn*))

(defvar *counter-fn* 1)
(defparameter *counter-fn* nil)
*counter-fn*

(defvar *x* 10)
(defun foo ()
  (format  t "Before assignement~18tX: ~d~%" *x*)
  (setf *x* (+ 1 *x*))
  (format  t "After assignement~18tX: ~d~%" *x*))

(defun bar ()
  (foo)
  (let ((*x* 20)) (foo))
  (foo))
(bar)

;; Assignment
(defun foo (x y)
  (setf x (setf y 10))
  (print x)
  (print y))
(let ((x 20) (y 20))
  (foo x y)
  (print x)
&allow-other-keys  (print y))

;;; 7. Macros: Standard Control Constructs
(defmacro my-when (condition &rest body)
  `(if ,condition (progn ,@body)))

(dolist (x '(1 2 3)) (print x))
(dolist (x '(1 2 3))
  (print x)
  (if (evenp x) (return)))
(dotimes (i 4) (print i))
(do ((i 0 (1+ i)))
    ((>= i 4))
  (print i))

(do ((n 0 (1+ n))
     (cur 0 next)
     (next 1 (+ cur next)))
    ((= 10 n) cur))

(loop for i from 1 to 10 collecting i)
(loop for x from 1 to 10 summing (expt x 2))
(loop for x across "the quick brown fox jumps over the lazy dog"
      counting (find x "aeiou"))
(loop for i below 10
      and a = 0 then b
      and b = 1 then (+ b a)
      finally (return a))

;;; 8. Macros: Defining Your Own
;; A Sample Macro: do-primes
(defun primep (number)
  (when (> number 1)
    (loop for fac from 2 to (isqrt number) never (zerop (mod number fac)))))

(defun next-prime (number)
  (loop for n from number when (primep n) return n))

;; we want this:
;; (do-primes (p 0 19)
;;   (format t "~d " p))
;;
;; to expand into this:
;; (do ((p (next-prime 0) (next-prime (1+ p))))
;;     ((> p 19))
;;   (format t "~d " p))

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for name in names collect `(,name (gensym)))
     ,@body))

(defmacro do-primes ((var start end) &body body)
  (with-gensyms (ending-value-name)
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
          (,ending-value-name ,end))
         ((> ,var ,ending-value-name))
       ,@body)))

(do-primes (p 0 19) (format t "~d " p))
(macroexpand-1 '(do-primes (p 0 19) (format t "~d " p)))
(do-primes (ending-value 0 10) (print ending-value))
(let ((ending-value 0))
  (do-primes (p 0 10)
    (incf ending-value p))
  ending-value)

;;; 9. Practical: Building a Unit Test Framework
(defvar *test-name* nil)

(defmacro deftest (name parameters &body body)
  `(defun ,name ,parameters
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))
(defmacro check (&body forms)
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))
(defmacro combine-results (&body forms)
  (with-gensyms (result)
    `(let ((,result t))
       ,@(loop for f in forms collect
               `(unless ,f (setf ,result nil)))
       ,result)))
(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" result *test-name* form)
  result)

(deftest test-+ ()
  (check
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)
    (= (+ -1 -3) -4)))
(deftest test-* ()
  (check
    (= (* 2 2) 4)
    (= (* 3 5) 15)))
(deftest test-arithmetic ()
  (combine-results
    (test-+)
    (test-*)))

(test-arithmetic)

(floor (/ -10 3))
(mod -10 3)

(truncate (/ -10 3))
(rem -10 3)

;;; 11. Collections
(vector)
(vector 1)
(vector 1 2)
(make-array 5 :initial-element nil)
(make-array 5 :fill-pointer 0)

(defparameter *x* (make-array 5 :fill-pointer 0))
(vector-push 'a *x*)
*x*
(vector-pop *x*)

(make-array 5 :fill-pointer 0 :adjustable t)
(defparameter *x* (make-array 5 :fill-pointer 0 :adjustable t))
(vector-push-extend 'a *x*)
*x*
(vector-pop *x*)

;; Subtypes of Vector
(make-array 5 :fill-pointer 0 :adjustable t :element-type 'character)
(make-array 5 :fill-pointer 0 :adjustable t :element-type 'bit)

;; Vectors As Sequences
(defparameter *x* (vector 1 2 3))
(length *x*)
(elt *x* 0)
(elt *x* 2)
;; fails
;; (elt *x* 3)
(setf (elt *x* 0) 10)
*x*

;; Sequence Iterating Functions
(count 1 #(1 2 1 2 3 1 2 3 4))
(remove 1 #(1 2 1 2 3 1 2 3 4))
(remove 1 '(1 2 1 2 3 1 2 3 4))
(remove #\a "foobarbaz")
(substitute 10 1 #(1 2 1 2 3 1 2 3 4))
(substitute 10 1 '(1 2 1 2 3 1 2 3 4))
(substitute #\x #\b "foobarbaz")
(find 1 #(1 2 1 2 3 1 2 3 4))
(find 10 #(1 2 1 2 3 1 2 3 4))
(position 1 #(1 2 1 2 3 1 2 3 4))

(count "foo" #("foo" "bar" "baz") :test #'string=)
(find 'c #((a 10) (b 20) (c 30) (d 40)) :key #'first)

(find 'a #((a 10) (b 20) (a 30) (b 40)) :key #'first)
(find 'a #((a 10) (b 20) (a 30) (b 40)) :key #'first :from-end t)

(remove #\a "foobarbaz" :count 1)
(remove #\a "foobarbaz" :count 1 :from-end t)

(count 'a
       #((a 10) (b 20) (a 30) (b 40))
       :key #'(lambda (x) (format t "Looking at ~s~%" x) (first x)))
(count 'a
       #((a 10) (b 20) (a 30) (b 40))
       :key #'(lambda (x) (format t "Looking at ~s~%" x) (first x))
       :from-end t)

;; Higher-Order Function Variants
(count-if #'evenp #(1 2 3 4 5))
(count-if-not #'evenp #(1 2 3 4 5))
(position-if #'digit-char-p "abcd0001")
(remove-if-not #'(lambda (x) (char= (elt x 0) #\f)) #("foo" "bar" "baz" "foom"))

(count-if #'evenp #((1 a) (2 b) (3 c) (4 d) (5 e)) :key #'first)
(count-if-not #'evenp #((1 a) (2 b) (3 c) (4 d) (5 e)) :key #'first)
(remove-if-not #'alpha-char-p #("foo" "bar" "1baz") :key #'(lambda (x) (elt x 0)))

(remove-duplicates #(1 2 1 2 3 1 2 3 4))
(remove-duplicates #((a 10) (b 20) (a 30) (b 40)) :key #'(lambda (x) (elt x 0)))
(remove-duplicates #((a 10) (b 20) (a 30) (b 40)) :key #'(lambda (x) (elt x 0)) :from-end t)

;; Whole Sequence Manipulations
(concatenate 'vector #(1 2 3) '(4 5 6))
(concatenate 'list #(1 2 3) '(4 5 6))
(concatenate 'string "abc" '(#\d #\e #\f))

;; Sorting and Merging
(sort (vector "foo" "bar" "baz") #'string<)

(defparameter *my-sequence* (vector "foo" "bar" "baz"))
(setf *my-sequence* (sort *my-sequence* #'string<))

(merge 'vector #(1 3 5) #(2 4 6) #'<)
(merge 'list #(1 3 5) #(2 4 6) #'<)

;; Subsequence Manipulations
(subseq "foobarbaz" 3)
(subseq "foobarbaz" 3 6)

(position #\b "foobarbaz")
(search "bar" "foobarbaz")

(mismatch "foobarbaz" "foom")
(mismatch "foobar" "bar" :from-end t)

;; Sequence Predicates
(every #'evenp #(1 2 3 4 5))
(some #'evenp #(1 2 3 4 5))
(notany #'evenp #(1 2 3 4 5))
(notevery #'evenp #(1 2 3 4 5))

(every #'> #(1 2 3 4) #(5 4 3 2))
(some #'> #(1 2 3 4) #(5 4 3 2))
(notany #'> #(1 2 3 4) #(5 4 3 2))
(notevery #'> #(1 2 3 4) #(5 4 3 2))

;; Sequence Mapping Functions
(map 'vector #'* #(1 2 3 4 5) #(10 9 8 7 6))
(reduce #'+ #(1 2 3 4 5 6 7 8 9 10))
(reduce #'+ #(1 2 3 4 5 6 7 8 9 10) :initial-value 0)


;; Hash Tables
(defparameter *h* (make-hash-table))
(gethash 'foo *h*)
(setf (gethash 'foo *h*) 'quux)
(gethash 'foo *h*)

(defun show-value (key hash-table)
  (multiple-value-bind (value present) (gethash key hash-table)
    (if present
        (format nil "Value ~a actually present." value)
        (format nil "Value ~a because key not found." value))))
(setf (gethash 'bar *h*) nil)
(show-value 'foo *h*)
(show-value 'bar *h*)
(show-value 'baz *h*)

;; Hash Table Iteration
(maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) *h*)

(loop for k being the hash-keys in *h* using (hash-value v)
      do (format t "~a => ~a~%" k v))

;; 12. They Called It LISP for a Reason: List Processing
(cons 1 2)
(car (cons 1 2))
(cdr (cons 1 2))

(defparameter *cons* (cons 1 2))
*cons*
(setf (car *cons*) 10)
*cons*
(setf (cdr *cons*) 20)
*cons*

(cons 1 nil)
(cons 1 (cons 2 nil))
(cons 1 (cons 2 (cons 3 nil)))

(defparameter *list* (list 1 2 3 4))
*list*
(first *list*)
(rest *list*)
(first (rest *list*))

(append (list 1 2) (list 3 4))

(defparameter *list-1* (list 1 2))
(defparameter *list-2* (list 3 4))
(defparameter *list-3* (append *list-1* *list-2*))
*list-1*
*list-2*
*list-3*
(setf (first *list-2*) 0)
*list-2*
*list-3*

(defparameter *x* (list 1 2 3))
(nconc *x* (list 4 5 6))
*x*

*list-2*
*list-3*
(setf *list-3* (delete 4 *list-3*))
*list-2*

;;; Mapping
(mapcar #'(lambda (x) (* 2 x)) (list 1 2 3))
(mapcar #'+ (list 1 2 3) (list 10 20 30))
(maplist #'(lambda (x) (* 2 (first x))) (list 1 2 3))
(mapcan #'(lambda (x) (list (* 2 x) (* 3 x))) (list 1 2 3))
(mapcon #'(lambda (x) (list (* 2 (first x)) (* 3 (first x)))) (list 1 2 3))
(mapc #'(lambda (x) (format t "~a~%" x)) (list 1 2 3))

;;; 13. Beyond Lists: Other Uses for Cons Cells
;; Trees
(subst 10 1 '(1 2 (3 2 1) ((1 1) (2 2))))

;; Sets
(defparameter *set* ())
(adjoin 1 *set*)
*set*
(setf *set* (adjoin 1 *set*))
*set*
(pushnew 2 *set*)
*set*
(pushnew 2 *set*)
*set*
(member 2 *set*)
(member 3 *set*)

(subsetp '(3 2 1) '(1 2 3 4))
(subsetp '(1 2 3 4) '(3 2 1))

;; Lookup Tables: Alists and Plists
(assoc 'a '((a . 1) (b . 2) (c . 3)))
(assoc 'd '((a . 1) (b . 2) (c . 3)))

(cdr (assoc 'a '((a . 1) (b . 2) (c . 3))))

(assoc "a" '(("a" . 1) ("b" . 2) ("c" . 3)) :test #'string=)

(assoc 'a '((a . 10) (a . 1) (b . 2) (c . 3)))
(assoc-if #'(lambda (x) (eq 'a x)) '((a . 10) (a . 1) (b . 2) (c . 3)))
(assoc-if-not #'(lambda (x) (eq 'a x)) '((a . 10) (a . 1) (b . 2) (c . 3)))

(rassoc 10 '((a . 10) (a . 1) (b . 2) (c . 3)))

(cons (cons 'new-key 'new-value) '((a . 10) (a . 1) (b . 2) (c . 3)))
(acons 'new-key 'new-value '((a . 10) (a . 1) (b . 2) (c . 3)))

(defparameter *alist-element1* '(a . 1))
(defparameter *alist-element2* '(b (e f)))
(defparameter *alist* `(,*alist-element1* ,*alist-element2* (c . 3)))
(defparameter *other-list* (copy-alist *alist*))
(setf (cdr *alist-element1*) 5)
(setf (first (second *alist-element2*)) 'g)
(setf (second (second *alist-element2*)) 'h)
*other-list*
*alist*

(pairlis '(a b c) '(1 2 3))

(defparameter *plist* ())
*plist*
(setf (getf *plist* :a) 1)
*plist*
(getf *plist* :a 3)
(getf *plist* :b 3)
(setf (getf *plist* :a) 2)
*plist*
(remf *plist* :a)
(remf *plist* :a)

(defun process-properties (plist keys)
  (loop while plist do
    (multiple-value-bind (key value tail) (get-properties plist keys)
      (when key (format t "~a~a~%" key value))
      (setf plist (cddr tail)))))
(setf (getf *plist* :a) 2)
(setf (getf *plist* :b) 3)
(setf (getf *plist* :c) 4)
(process-properties *plist* '(:b :c))

(symbol-plist :a)
(setf (get :a 'my-key) "information")

(getf (symbol-plist :a) 'my-key)
(get :a 'my-key)
(get ':a 'my-key)
;; not the same
(get 'a 'my-key)

(remf (symbol-plist :a) 'my-key)
(remprop :a 'my-key)

;; DESTRUCTURING-BIND
(destructuring-bind (x y z) (list 1 2 3)
  (list :x x :y y :z z))
(destructuring-bind (x y z) '(1 2 3)
  (list :x x :y y :z z))
(destructuring-bind (x y z) (list 1 (list 2 20) 3)
  (list :x x :y y :z z))
(destructuring-bind (x (y1 y2) z) (list 1 (list 2 20) 3)
  (list :x x :y1 y1 :y2 y2 :z z))
(destructuring-bind (x (y1 &optional y2) z) (list 1 (list 2 20) 3)
  (list :x x :y1 y1 :y2 y2 :z z))
(destructuring-bind (x (y1 &optional y2) z) (list 1 (list 2) 3)
  (list :x x :y1 y1 :y2 y2 :z z))
(destructuring-bind (&key x y z) (list :x 1 :y 2 :z 3)
  (list :x x :y y :z z))
(destructuring-bind (&key x y z) (list :z 1 :y 2 :x 3)
  (list :x x :y y :z z))

(destructuring-bind (&whole whole &key x y z) (list :z 1 :y 2 :x 3)
  (list :x x :y y :z z :whole whole))

;;; 14. Files and File I/O
;; Reading File Data
(let ((in (open "README.md")))
  (format t "~a~%" (read-line in))
  (close in))
(let ((in (open "/some/file/name.txt" :if-does-not-exist nil)))
  (when in
    (format t "~a~%" (read-line in))
    (close in)))
(let ((in (open "README.md" :if-does-not-exist nil)))
  (when in
    (loop for line = (read-line in nil)
          while line do (format t "~a~%" line))
    (close in)))

;; Closing Files
(let ((to-print '((1 2 3) 456 "a string ; this is a comment" ((a b) (c d)))))
  (with-open-file (out "tmp" :direction :output :if-exists :supersede)
    (when out
      (loop for el in to-print do (print el out)))))

(defparameter *s* (open "tmp"))
(read *s*)
(read *s*)
(read *s*)
(read *s*)
(close *s*)

;; How Pathnames Represent Filenames
(pathname "/foo/bar/baz.txt")
(pathname-directory (pathname "foo/bar/baz.txt"))
(pathname-directory (pathname "/foo/bar/baz.txt"))
(pathname-name (pathname "/foo/bar/baz.txt"))
(pathname-type (pathname "/foo/bar/baz.txt"))

(pathname-host (pathname "/foo/bar/baz.txt"))
(pathname-device (pathname "/foo/bar/baz.txt"))
(pathname-version (pathname "/foo/bar/baz.txt"))

(namestring (pathname "/foo/bar/baz.txt"))
(directory-namestring (pathname "/foo/bar/baz.txt"))
(file-namestring (pathname "/foo/bar/baz.txt"))
(wild-pathname-p (make-pathname :name :wild :defaults (pathname "foo/bar/baz")))

;; Constructing New Pathnames
(make-pathname
 :directory '(:absolute "foo" "bar")
 :name "baz"
 :type "txt")

(make-pathname :directory '(:relative "backups") :defaults #p"baz.txt")
(make-pathname :directory '(:relative "backups") :defaults #p"/foo/bar/baz.txt")
(merge-pathnames #p"foo/bar.html" #p"/www/html/")
(merge-pathnames #p"foo/bar.html" #p"html/")
(enough-namestring #p"/www/html/foo/bar.html" #p"/www/")
(merge-pathnames
 (enough-namestring #p"/www/html/foo/bar/baz.html" #p"/www/")
 #p"/www-backups/")
(merge-pathnames #p"foo.txt")

;; Interacting with the File System
(with-input-from-string (s "1.23")
  (read s))
(with-output-to-string (out)
  (format out "hello, world ")
  (format out "~s" (list 1 2 3)))

;;; 15. Practical: A Portable Pathname Library
(defun foo () #+allegro 0
  #+sbcl 1
  #+clisp 2
  #+cmu 3
  #-(or allegro sbcl clisp cmu) (error "Not implemented"))

(in-package :cl-user)

(defpackage :com.gigamonkeys.pathnames
  (:use :common-lisp)
  (:export
   :list-directory
   :file-exists-p
   :directory-pathname-p
   :file-pathname-p
   :pathname-as-directory
   :pathname-as-file
   :walk-directory
   :directory-p
   :file-p))

(in-package :com.gigamonkeys.pathnames)

;; Listing a Directory
(defun component-present-p (value)
  (and value (not (eql value :unspecific))))
(defun directory-pathname-p  (p)
  (and
   (not (component-present-p (pathname-name p)))
   (not (component-present-p (pathname-type p)))
   p))

(defun pathname-as-directory (name)
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (directory-pathname-p pathname)
        pathname
        (make-pathname
         :directory (append
                     (or (pathname-directory pathname) (list :relative))
                     (list (pathname-name pathname)))
         :name nil
         :type nil
         :defaults pathname))))
(defun directory-wildcard (dirname)
  (make-pathname
   :name :wild
   :type #-clisp :wild #+clisp nil
   :defaults (pathname-as-directory dirname)))
#+clisp
(defun clisp-subdirectories-wildcard (wildcard)
  (make-pathname
   :directory (append (pathname-directory wildcard) (list :wild))
   :name nil
   :type nil
   :defaults wildcard))
(defun list-directory (dirname)
  (when (wild-pathname-p dirname)
    (error "Can only list concrete directory names."))
  (let ((wildcard (directory-wildcard dirname)))

    #+(or sbcl cmu lispworks)
    (directory wildcard)

    #+openmcl
    (directory wildcard :directories t)

    #+allegro
    (directory wildcard :directories-are-files nil)

    #+clisp
    (nconc
     (directory wildcard)
     (directory (clisp-subdirectories-wildcard wildcard)))
    #-(or sbcl cmu lispworks openmcl allegro clisp)
    (error "list-directory not implemented")))

(list-directory ".")
(list-directory "/does/not/exist")

(pathname-directory (pathname "."))
(pathname-directory (probe-file (pathname ".")))
(last (pathname-directory (probe-file (pathname "."))))
(first (last (pathname-directory (probe-file (pathname ".")))))
(pathname (first (last (pathname-directory (probe-file (pathname "."))))))
(pathname-directory (pathname (first (last (pathname-directory (probe-file (pathname ".")))))))
(pathname-name (pathname (first (last (pathname-directory (probe-file (pathname ".")))))))

;; Testing a File's Existence
(defun pathname-as-file (name)
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (directory-pathname-p pathname)
        (let* ((directory (pathname-directory pathname))
               (name-and-type (pathname (first (last directory)))))
         (make-pathname
          :directory (butlast directory)
          :name (pathname-name name-and-type)
          :type (pathname-type name-and-type)
          :defaults pathname))
        pathname)))
(defun file-exists-p (pathname)
  #+(or sbcl lispworks openmcl)
  (probe-file pathname)

  #+(or allegro cmu)
  (or (probe-file (pathname-as-directory pathname))
      (probe-file (pathname-as-file pathname)))

  #+clisp
  (or (ignore-errors
       (probe-file (pathname-as-file pathname)))
      (ignore-errors
       (let ((directory-form (pathname-as-directory pathname)))
         (when (ext:probe-directory directory-form)
           directory-form))))

  #-(or sbcl lispworks openmcl allegro cmu clisp)
    (error "file-exists-p not implemented"))

;; Walking a Directory Tree
(defun walk-directory (dirname fn &key directories (test (constantly t)))
  (labels
      ((walk (name)
         (cond
           ((directory-pathname-p name)
            (when (and directories (funcall test name))
              (funcall fn name))
            ; NOTE: does not call `test' on this recursive call
            (dolist (x (list-directory name)) (walk x)))
           ((funcall test name) (funcall fn name)))))
    (walk (pathname-as-directory dirname))))

(walk-directory "." (lambda (name) (format t "~a~%" name)))
(walk-directory "." (lambda (name) (format t "~a~%" name)) :directories nil)
(walk-directory "." (lambda (name) (format t "~a~%" name))
                :test (lambda (name) (char= (elt (first (last (pathname-directory name))) 0) #\.)))
(walk-directory "." (lambda (name) (format t "~a~%" name))
                :directories nil
                :test (lambda (name) (char= (elt (first (last (pathname-directory name))) 0) #\.)))

(in-package :cl-user)
;;; 16. Object Reorientation: Generic Functions
;;; 17. Object Reorientation: Classes
(defclass bank-account ()
  (customer-name
   balance))
(make-instance 'bank-account)

(defparameter *account* (make-instance 'bank-account))
(setf (slot-value *account* 'customer-name) "John Doe")
(setf (slot-value *account* 'balance) 1000)

(slot-value *account* 'customer-name)
(slot-value *account* 'balance) 

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name)
   (balance
    :initarg :balance
    :initform 0)))

(defparameter *account*
  (make-instance 'bank-account :customer-name "Jane" :balance 1000))
(slot-value (make-instance 'bank-account) 'balance)
;; error
;; (slot-value (make-instance 'bank-account) 'customer-name)

(defvar *account-numbers* 0)

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name
    :initform (error "Must supply a customer name."))
   (balance
    :initarg :balance
    :initform 0)
   (account-number
    :initform (incf *account-numbers*))))

(slot-value (make-instance 'bank-account :customer-name "Jane") 'account-number)

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name
    :initform (error "Must supply a customer name."))
   (balance
    :initarg :balance
    :initform 0)
   (account-number
    :initform (incf *account-numbers*))
   account-type))

(defmethod initialize-instance :after ((account bank-account) &key opening-bonus-percentage)
  (let ((balance (slot-value account 'balance)))
    (setf (slot-value account 'account-type)
          (cond
            ((>= balance 100000) :gold)
            ((>= balance 50000) :silver)
            (t :bronze))))

  (when opening-bonus-percentage
    (incf (slot-value account 'balance)
          (* (slot-value account 'balance) (/ opening-bonus-percentage 100)))))

(slot-value (make-instance 'bank-account :customer-name "Jane") 'account-type)
(slot-value (make-instance 'bank-account :customer-name "Jane" :balance 50000) 'account-type)
(slot-value (make-instance 'bank-account :customer-name "Jane" :balance 100000) 'account-type)
(slot-value
 (make-instance
             'bank-account
             :customer-name "Jane"
             :balance 100000
             :opening-bonus-percentage 5)
 'balance)

;; Accessor Function
(defgeneric balance (account))

(defmethod balance ((account bank-account))
  (slot-value account 'balance))

(defgeneric (setf customer-name) (value account))

(defmethod (setf customer-name) (name (account bank-account))
  (setf (slot-value account 'customer-name) name))

(defgeneric customer-name (account))

(defmethod customer-name ((account bank-account))
  (slot-value account 'customer-name))

(setf (customer-name *account*) "Sally Sue")
(customer-name *account*)

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name
    :initform (error "Must supply a customer name.")
    :reader customer-name
    :writer (setf customer-name))
   (balance
    :initarg :balance
    :initform 0
    :reader balance)
   (account-number
    :initform (incf *account-numbers*))
   account-type))

(balance (make-instance 'bank-account :customer-name "Jane"))
(setf (customer-name (make-instance 'bank-account :customer-name "Jane")) "new-name")

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name
    :initform (error "Must supply a customer name.")
    :accessor customer-name
    :documentation "Customer's name")
   (balance
    :initarg :balance
    :initform 0
    :reader balance
    :documentation "Current account balance")
   (account-number
    :initform (incf *account-numbers*)
    :reader account-number
    :documentation "Account number, unique within a bank.")
   (account-type
    :reader account-type
    :documentation "Type of account, one of :gold, :silver, or :bronze.")))

;; WITH-SLOTS and WITH-ACCESSORS
(defparameter *minimum-balance* 10)

(defmethod assess-low-balance-penalty ((account bank-account))
  (when (< (balance account) *minimum-balance*)
    (decf (slot-value account 'balance) (* (balance account) .01))))

(defmethod assess-low-balance-penalty ((account bank-account))
  (when (< (slot-value account 'balance) *minimum-balance*)
    (decf (slot-value account 'balance) (* (slot-value account 'balance) .01))))

(defmethod assess-low-balance-penalty ((account bank-account))
  (with-slots (balance) account
    (when (< balance *minimum-balance*)
      (decf balance (* balance .01)))))

(assess-low-balance-penalty (make-instance 'bank-account :customer-name "sam" :balance 5))

(defmethod print-append-customer-name ((account bank-account))
  (with-accessors ((cname customer-name)) account
    (format t "~a" cname)
    (setf cname "new-name")))

(print-append-customer-name (make-instance 'bank-account :customer-name "Sam"))

;; Slots and Inheritance
(defclass foo ()
  ((a :initarg :a :initform "A" :accessor a)
   (b :initarg :b :initform "B" :accessor b)))

(defclass bar (foo)
  ((a :initform (error "Must supply a value for a"))
   (b :initarg :the-b :accessor the-b :allocation :class)))

(a (make-instance 'foo :a "My fooA"))
(a (make-instance 'bar :a "My barA"))
(b (make-instance 'foo :b "My fooB"))
(b (make-instance 'bar :a "a" :b "My barB"))
(b (make-instance 'bar :a "a" :the-b "My barB"))
(b (make-instance 'bar :a "a" :b "class-wide b"))
(b (make-instance 'bar :a "a"))

;; Multiple Inheritance
(defclass money-market-account (checking-account savings-account) ())

;;; 18. A Few FORMAT Recipes
;; FORMAT Directives
(format t "~$" pi)
(format t "~5$" pi)
(format t "~v$" 3 pi)
(format t "~#$" pi)
(format t "~#$" pi 1)
(format t "~,5f" pi)
(format t "~d" 1000000)
(format t "~:d" 1000000)
(format t "~@d" 1000000)
(format t "~@:d" 1000000)
(format t "~:@d" 1000000)

;; Basic Formatting
;; ~a for aesthetic
(format nil "The value is: ~a" 10)
(format nil "The value is: ~a" "foo")
(format nil "The value is: ~a" (list 1 2 3))

;; ~s for output which can be read back with READ
(format nil "The value is: ~s" 10)
(format nil "The value is: ~s" "foo")
(format nil "The value is: ~s" (list 1 2 3))

(format nil "~a" nil)
(format nil "~:a" nil)
(format t "~%")
(format t "~%")
(format t "~&")
(format t "~&")

(format t "~3&")
(format t "~3%")

;; Character and Integer Directives
(format t "Syntax error. Unexpected character: ~:c" #\Space)
(format t "Syntax error. Unexpected character: ~:c" #\ )
(format t "Syntax error. Unexpected character: ~:c" #\a)
(format t "Syntax error. Unexpected character: ~@c~%" #\a)
(format t "Syntax error. Unexpected character: ~@c~%" #\Space)
(format t "Syntax error. Unexpected character: ~@:c~%" #\a)
(format t "Syntax error. Unexpected character: ~@:c~%" #\Space)

(format nil "~d" 1000000)
(format nil "~:d" 1000000)
(format nil "~@d" 1000000)
(format nil "~@:d" 1000000)

(format nil "~12d" 1000000)
(format nil "~12,'0d" 1000000)

;; Floating-Point Directives
(format nil "~f" pi)
(format nil "~,4f" pi)
(format nil "~e" pi)
(format nil "~,4e" pi)

(format nil "~$" pi)
(format nil "~2,4$" pi)
(format nil "~2,4@$" pi)

;; English-Language Directives

(format nil "~r" 1234)
(format nil "~:r" 1234)
(format nil "~@r" 1234)
(format nil "~@:r" 1234)

(format nil "file~p" 1)
(format nil "file~p" 10)
(format nil "file~p" 0)

(format nil "~r file~:p" 1)
(format nil "~r file~:p" 10)
(format nil "~r file~:p" 0)

(format nil "~r famil~:@p" 1)
(format nil "~r famil~:@p" 10)
(format nil "~r famil~:@p" 0)

(format nil "~(~a~)" "FOO")
(format nil "~(~@r~)" 124)

(format nil "~(~a~)" "tHe Quick BROWN foX")
(format nil "~@(~a~)" "tHe Quick BROWN foX")
(format nil "~:(~a~)" "tHe Quick BROWN foX")
(format nil "~:@(~a~)" "tHe Quick BROWN foX")

;; Conditional Formatting
(format nil "~[cero~;uno~;dos~]" 0)
(format nil "~[cero~;uno~;dos~]" 1)
(format nil "~[cero~;uno~;dos~]" 2)
(format nil "~[cero~;uno~;dos~]" 3)
(format nil "~[cero~;uno~:;dos~]" 3)

(defparameter *list-etc*
  "~#[NONE~;~a~;~a and ~a~:;~a, ~a~]~#[~; and ~a~:;, ~a, etc~].")
(format nil *list-etc*)
(format nil *list-etc* 'a)
(format nil *list-etc* 'a 'b)
(format nil *list-etc* 'a 'b 'c)
(format nil *list-etc* 'a 'b 'c 'd)
(format nil *list-etc* 'a 'b 'c 'd 'e)

(format nil "~:[FAIL~;pass~]" ())
(format nil "~:[FAIL~;pass~]" t)

(format nil "~@[x = ~a ~]~@[y = ~a~]" 10 20)
(format nil "~@[x = ~a ~]~@[y = ~a~]" 10 nil)
(format nil "~@[x = ~a ~]~@[y = ~a~]" nil 20)
(format nil "~@[x = ~a ~]~@[y = ~a~]" nil nil)

;; Iteration
(format nil "~{~a, ~}" (list 1 2 3))
(format nil "~{~a~^, ~}" (list 1 2 3))
(format nil "~@{~a~^, ~}" 1 2 3)
(format nil "~{~a~#[~;, and ~:;, ~]~}" (list 1 2 3))
(format nil "~{~a~#[~;, and ~:;, ~]~}" (list 1 2))

(defparameter *english-list*
  "~{~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]~}")
(format nil *english-list* '())
(format nil *english-list* '(1))
(format nil *english-list* '(1 2))
(format nil *english-list* '(1 2 3))
(format nil *english-list* '(1 2 3 4))

(apply #'format nil "~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]" '())
(apply #'format nil "~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]" '(1))
(apply #'format nil "~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]" '(1 2))
(apply #'format nil "~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]" '(1 2 3))
(apply #'format nil "~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]" '(1 2 3 4))

(format nil   "~{~#[<empty>~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]~:}" '())
(format nil   "~:{~a~a ~}" '((1 2) (3 4)))

;; Hop, Skip, Jump
(format nil "~*~a" 1 2)
(format nil "~r ~:*(~d)" 1)

(format nil "I saw ~r el~:*~[ves~;f~:;ves~]." 0)
(format nil "I saw ~r el~:*~[ves~;f~:;ves~]." 1)
(format nil "I saw ~r el~:*~[ves~;f~:;ves~]." 2)

(format nil "I saw ~[no~:;~:*~r~] el~:*~[ves~;f~:;ves~]." 0)
(format nil "I saw ~[no~:;~:*~r~] el~:*~[ves~;f~:;ves~]." 1)
(format nil "I saw ~[no~:;~:*~r~] el~:*~[ves~;f~:;ves~]." 2)

(format nil "~{~s~*~^ ~}" '(:a 10 :b 20))

;;; 19. Beyond Exception Handling: Conditions and Restarts
;; Conditions
(define-condition malformed-log-entry-error (error)
  ((text :initarg :text :reader text)))

;; Condition Handlers
(defclass log-entry ()
  ((text
    :initarg :text
    :initform (error "Must supply log text")
    :reader text)))

(defun well-formed-log-entry-p (text)
  (> (length text) 0))

(well-formed-log-entry-p "")
(well-formed-log-entry-p "a")

(defun parse-log-entry (text)
  (if (well-formed-log-entry-p text)
      (make-instance 'log-entry :text text)
      (error 'malformed-log-entry-error :text text)))

(handler-case 1
  (malformed-log-entry-error (e) (print (text e))))
(handler-case (error 'malformed-log-entry-error :text "test error")
  (malformed-log-entry-error (e) (text e)))

(defun parse-log-file (file)
  (with-open-file (in file :direction :input)
    (loop for text = (read-line in nil nil) while text
          for entry = (handler-case (parse-log-entry text)
                        (malformed-log-entry-error () nil))
          when entry collect it)))

(dolist (log (parse-log-file "README.md"))
  (print (text log)))

;; Restarts
(defun parse-log-file (file)
  (with-open-file (in file :direction :input)
    (loop for text = (read-line in nil nil) while text
          for entry = (restart-case (parse-log-entry text)
                        (skip-log-entry () nil))
          when entry collect it)))
(dolist (log (parse-log-file "README.md"))
  (print (text log)))

(defun find-all-logs () '("README.md" "loadup.lisp"))
(defun analyze-entry (log)
  (print (text log)))
(defun analyze-log (file)
  (dolist (entry (parse-log-file file))
    (analyze-entry entry)))
(defun log-analyzer ()
  (handler-bind ((malformed-log-entry-error
                   #'(lambda (c)
                       (declare (ignore c))
                       (invoke-restart 'skip-log-entry))))
    (dolist (log (find-all-logs))
      (analyze-log log))))

(defun skip-log-entry (c)
  (declare (ignore c))
  (invoke-restart 'skip-log-entry))
(defun log-analyzer ()
  (handler-bind ((malformed-log-entry-error
                   #'skip-log-entry))
    (dolist (log (find-all-logs))
      (analyze-log log))))

(defun skip-log-entry (c)
  (declare (ignore c))
  (let ((restart (find-restart 'skip-log-entry)))
    (when restart (invoke-restart restart))))

(log-analyzer)

;; Providing Multiple Restarts
(defclass malformed-log-entry ()
  ((text :initarg :text :reader text)))

(defun parse-log-entry (text)
  (if (well-formed-log-entry-p text)
      (make-instance 'log-entry :text text)
      (restart-case (error 'malformed-log-entry-error :text text)
        (use-value (value) value)
        (reparse-entry (fixed-text) (parse-log-entry fixed-text)))))

(defun log-analyzer ()
  (handler-bind ((malformed-log-entry-error
                   #'(lambda (c)
                       (use-value
                        (make-instance 'malformed-log-entry
                                       :text (concatenate 'string
                                                          "Malformed Log Entry: \"\""
                                                          (text c)))))))
    (dolist (log (find-all-logs))
      (analyze-log log))))

(log-analyzer)

;;; 20. The Special Operators
(defun collect-leaves (tree)
  (let ((leaves ()))
    (labels ((walk (tree)
               (cond
                 ((null tree))
                 ((atom tree) (push tree leaves))
                 (t (walk (car tree))
                    (walk (cdr tree))))))
      (walk tree))
    (nreverse leaves)))

;; (with-slots (x y z) foo (list x y z))
;; ->
;; (with-gensyms (instance)
;;   (let ((instance foo))
;;     (symbol-macrolet
;;         ((x (slot-value instance 'x))
;;          (y (slot-value instance 'y))
;;          (z (slot-value instance 'z)))
;;       (list x y z))))
(defmacro my-with-slots ((&rest names) instance &body body)
  (with-gensyms (inst)
    `(let ((,inst ,instance))
       (symbol-macrolet
           ,(loop for name in names collect `(,name (slot-value instance ',name)))
           ,@body))))

;; Local Flow of Control
(dotimes (i 10)
  (let ((answer (random 100)))
    (print answer)
    (if (> answer 50) (return))))

(block some-name
  (return-from some-name 3)
  5)

(tagbody
 top
   (print 'hello)
   (when (plusp (random 10)) (go top)))

(tagbody
 a (print 'a) (if (zerop (random 2)) (go c))
 b (print 'b) (if (zerop (random 2)) (go a))
 c (print 'c) (if (zerop (random 2)) (go b)))

;; Unwinding the Stack
(defun foo ()
  (format t "Entering foo~%")
  (block a
    (format t " Entering BLOCK~%")
    (bar #'(lambda () (return-from a)))
    (format t " Leaving BLOCK~%"))
  (format t "Leaving foo~%"))
(defun bar (fn)
  (format t "  Entering bar~%")
  (baz fn)
  (format t "  Leaving bar~%"))
(defun baz (fn)
  (format t "   Entering baz~%")
  (funcall fn)
  (format t "   Leaving baz~%"))
(foo)

(defparameter *obj* (cons nil nil))
(defun foo ()
  (format t "Entering foo~%")
  (catch *obj*
    (format t " Entering CATCH~%")
    (bar)
    (format t " Leaving CATCH~%"))
  (format t "Leaving foo~%"))
(defun bar ()
  (format t "  Entering bar~%")
  (baz)
  (format t "  Leaving bar~%"))
(defun baz ()
  (format t "   Entering baz~%")
  (throw *obj* nil)
  (format t "   Leaving baz~%"))
(foo)

(defmacro with-database-connection ((var &rest open-args) &body body)
  `(let ((,var (open-connection ,@open-args)))
     (unwind-protect (progn ,@body)
       (close-connection ,var))))

;; Multiple Values
(values-list '(1 2))
(values 1 2)
(apply #'values '(1 2))
(funcall #'values 1 2)

(funcall #'+ (values 1 2) (values 3 4))
(multiple-value-call #'+ (values 1 2) (values 3 4))

(multiple-value-bind (x y) (values 1 2)
  (+ x y))

(multiple-value-list (values 1 2))
(values-list (multiple-value-list (values 1 2)))

(defparameter *x* nil)
(defparameter *y* nil)
(setf (values *x* *y*) (floor (/ 57 34)))
*x*
*y*

;; Other Special Operators
(defun when-loaded () (load-time-value (get-universal-time)))
(when-loaded)

(progv (if t '(a b) '(c d)) '(1 2)
  `(,a ,b))

;;; 21. Programming in the Large: Packages and Symbols
(find-package 'cl)
(find-package 'cl-user)
(find-package 'foo)

(find-symbol "*X*")
(find-symbol "FOO")

(eql ':foo :foo)
(symbol-name :foo)
(symbol-name '*x*)

(eql '#:foo '#:foo)
(symbol-name '#:foo)
'#:foo

(package-name 'cl-user)
(package-name *package*)

*package*
common-lisp:*package*
cl:*package*

;; Three Standard Packages
(defvar *x* 10)
(symbol-package '*x*)
(package-name (symbol-package '*x*))
(symbol-package 'foo)
(package-name (symbol-package 'mapcar))
cl-user::*x*
;; TODO: why isn't this working
(find-symbol "COMMON-LISP-USER::*X*")

(package-use-list :cl-user)
(mapcar #'package-name (package-use-list :cl-user))

:a
keyword:a
(eql :a keyword:a)

;; Defining Your Own Packages
;; it's common to use kw symbols
(defpackage :com.gigamonkeys.email-db
  (:use :common-lisp))
;; but we could use strings instead, though they won't be automatically case converted
;; and it would be annoying to use if we defined it as lowercase
(defpackage "COM.GIGAMONKEYS.EMAIL-DB"
  (:use "COMMON-LISP"))
;; The arguments to defpackage aren't actually evaluated so we can give a variable that doesn't exist.
;; To avoid the reader interning symbols unnecessarily, we can give an uninterned symbol, which naturally
;; is not bound to anything
(defpackage #:com.gigamonkeys.email-db
  (:use #:common-lisp))
;; TODO: I assume this doesn't work because the expression doesn't actually get
;; evaluated so it somehow doesn't resolve to an *x*???
;; (defpackage ':com.gigamonkeys.email-db
;;   (:use ':common-lisp))

(package-name :common-lisp)
(package-name :cl)
(package-name "COMMON-LISP")
(package-name "CL")
; (package-name "common-lisp") wouldn't work

(in-package :cl-user)
(defun hello-world () (format t "hello, world~%"))

(in-package :com.gigamonkeys.email-db)
*package*
(cl-user::hello-world)

(defun hello-world () (format t "hello from EMAIL-DB package~%"))
(hello-world)

(in-package :cl-user)
(hello-world)

;; Packaging Reusable Libraries
(defpackage :com.gigamonkeys.text-db
  (:use :cl)
  (:export :open-db
   :save
           :store))
(defpackage :com.gigamonkeys.email-db
  (:use :cl :com.gigamonkeys.text-db))

;; Importing Individual Names
(defpackage :com.acme.email
  (:use :cl)
  (:export :some-conflicting-symbols-for-example
   :send-email
           :we-only-want-parse-email-address
   :parse-email-address
           :build-index))
(defpackage :com.acme.text
  (:use :cl)
  (:export :something-useful
   :something-else
           :build-index
           :save))
(defpackage :com.gigamonkeys.email-db
  (:use
   :cl
   :com.gigamonkeys.text-db
   :com.acme.text)
  (:import-from :com.acme.email :parse-email-address)
  (:shadow :build-index)
  (:shadowing-import-from :com.gigamonkeys.text-db :save))

;; Packaging Mechanics
;; For simple programs, you can manually load each file in the right order. It is
;; good practice to only use one in-package per file (at the top of the file) to
;; avoid confusing developers and sometimes even tooling.
;; For bigger programs, you'll want to use a "system definition facility" like "asdf".
(compile-file "packages.lisp")
(load "packages.fasl")

;; Package Gotchas
(package-name (symbol-package 'quit))

;;; 22. LOOP for Black Belts
;; Iteration Control
(loop
      for item in '(a b c)
      for i from 1 to 10
      do (format t "~a ~a~%" item i))

;; Counting Loops
(loop for i upto 10 collect i)
                                        ; (loop for i downto -10 collect i) won't work, there's no default `from` when using downto
(loop for i from 0 downto -10 collect i)
(loop for i from 10 to 20 collect i)
;; i is already greater than 10 and loop doesn't know that we want to step down, so this doesn't run
(loop for i from 20 to 10 collect i)
;; instead do this
(loop for i downfrom 20 to 10 collect i)
;; or this
(loop for i from 20 downto 10 collect i)

;; for just doing something a number of times do this
(loop repeat 3 do (format t "repeating~%"))

;; Looping Over Collections and Packages
(loop for i in (list 10 20 30 40) collect i)
(loop for i in (list 10 20 30 40) by #'cdr collect i)
(loop for i in (list 10 20 30 40) by #'cddr collect i)
(loop for i on (list 10 20 30) collect i)
(loop for i on (list 10 20 30) by #'cddr collect i)

(loop for x across "abcd" collect x)
(loop for x across #(1 2 3) collect x)

(loop for k being the symbols in :com.gigamonkeys.text-db do (format t "~a~%" k))
(loop for k being the present-symbols in :com.gigamonkeys.text-db do (format t "~a~%" k))
(loop for k being the external-symbols in :com.gigamonkeys.text-db do (format t "~a~%" k))

;; Equals-Then Iteration
(loop repeat 5
      for x = 0 then y
      for y = 1 then (+ x y)
      collect y)
(loop repeat 5
      for x = 0 then y
      and y = 1 then (+ x y)
      collect y)
(loop repeat 3
      with x = 0
      with y = (+ x 1)
      collect (list x y)
      do (setf x y) (setf y (+ 1 y)))
(loop repeat 3
      with x = 0
      and y = 1 ; can't reference x if we use "and"
      collect (list x y)
      do (setf x y) (setf y (+ 1 y)))

;; Destructuring Variables
(loop for (a b) in '((1 2) (3 4) (5 6))
      do (format t "a: ~a; b: ~a~%" a b))

(loop for cons on '(1 2 3 4 5)
      do (format t "~a" (car cons))
      when (cdr cons) do (format t ", "))
; can be replaced with
(loop for (item . rest) on '(1 2 3 4 5)
      do (format t "~a" item)
      when rest do (format t ", "))

(loop for (a nil) in '((1 2) (3 4) (5 6)) collect a)
(loop for (a b) in '((1) (2) (3)) collect `(,a ,b))

;; Value Accumulation
(loop for x in '(1 2 3) collect x)
(loop for x in '(1 2 3) collecting x) ; the same thing
(loop for x on '(1 2 3) append x)
(loop for x in '(1 2 3) nconc (list x)) ; nconc is destructive so we can't use the same example as above
(loop for x in '(t nil t) count x)

(loop for i in (loop repeat 100 collect (random 10000))
      counting (evenp i) into evens
      counting (oddp i) into odds
      summing i into total
      maximizing i into max
      minimizing i into min
      finally (return (list min max total evens odds)))

;; Unconditional Execution
(block outer
  (loop for i from 0 return 100)
  (print "This will print")
  200)
(block outer
  (loop for i from 0 do (return-from outer 100))
  (print "This will not print")
  200)

;; Conditional Execution
(loop for i from 1 to 10 when (evenp i) sum i)
(loop for i from 1 to 10 when (evenp i)
      sum i
      and do (print i))
(loop for i from 1 to 10 when (evenp i) when (< i 6) collect i)

(defparameter *h* (make-hash-table))
(setf (gethash 1 *h*) 'a)
(setf (gethash 2 *h*) nil)
(setf (gethash 3 *h*) 'c)
(loop for key in '(1 2 3) when (gethash key *h*) collect it)

(loop for i from 1 to 10 when (evenp i) collect i else collect nil)
(loop for i from 1 to 10
      when (evenp i) collect i and
        when (> i 5) collect (- 0 i) and do (print (- 0 i)))
(loop for i from 1 to 10
      when (evenp i) collect i and
        when (> i 5) collect (- 0 i) end and do (print (- 0 i)))

(loop named outer for list in '((1 2 3) (4 5 6) (7 8 9)) do
  (loop for item in list do
    (if (= 5 item)
        (return-from outer item))))

;; Setting Up and Tearing Down
(loop repeat 5
      with i = 0
        initially (incf i)
      do (print i)
      do (incf i)
      finally (return i))

;; Termination Tests
(loop for n in '(2 4 6 8) always (evenp n))
(loop for n in '(2 4 6 8) always (evenp n) do (print n))
(loop for n in '(2 4 5 8) always (evenp n) do (print n))

;;; 23. Practical: A Spam Filter
